package com.yara.model.core;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "YARA_USER")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "USER_ID")
    private String userId;

    @Column(nullable = false, name = "PASSWORD")
    private String password;

    @Column(nullable = false, name = "CREATED_AT")
    private Date createdAt;

    @Column(nullable = false, name = "FIRST_LOGIN")
    private boolean firtsLogin;

    @Column(nullable = false, name = "LOGIN_EMAIL")
    private boolean loginMail;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "PERSON")
    private Person person;

}
