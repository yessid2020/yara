package com.yara.model.core;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "YARA_PERSON")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "IDENTIFICATION")
    private Integer identification;

    @Column(nullable = false, name = "NAME")
    private String name;

    @Column(nullable = false, name = "JOB")
    private String job;

    @Column(nullable = false, name = "CITY")
    private String city;

}
