package com.yara.persistence.core;

import com.yara.model.core.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

public interface UserDao extends JpaRepository<User, Long> {

    Optional<User> findById(final Long id);
}
